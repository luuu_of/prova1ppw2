const express = require('express')
const app = express()
const PORT = 1234
const dados = require('./dados')

app.listen(PORT, function(){
    console.log(`Servidor iniciado na porta ${PORT}`)
})

app.get('/api/jogador/random', function(req, res){

    const filtro = req.query.filtro

    var jogador = criarJogador()

    jogador.mensagem = `${jogador.nome} ${jogador.sobrenome} é um futebolista brasileiro de ${jogador.idade} anos que atua como ${jogador.posicao}. Atualmente defende o ${jogador.clube}.`

    if(filtro == "idade"){
        if(jogador.idade <= 21){
            jogador.idade = "Novato"
        } else {
            if(jogador.idade <= 28){
                jogador.idade = "Profissional"
            } else {
                if(jogador.idade <= 34){
                    jogador.idade = "Veterano"
                } else {
                    jogador.idade = "master"
                }
            }
        }
    }
    res.json(jogador)
})

router.get("api/jogador", function (req, res) {
    let limite = req.query.limite;
    let listaJogador = [];
    if (limite == undefined) {
        let jogador = gerarJogador();
        for (let i = 0; i <= 5; i++) {
            listaJogador.push(jogador);
        }
    } else {
        for (let i = 0; i < limite; i++) {
            let jogador = gerarJogador();
            listaJogador.push(jogador);
        }
    }

    res.json(listaJogador);
})

app.post('/api/inserir/:prop?valor=:valor', function(req, res){
   
    if(req.params.prop && req.params.valor){
        let prop = req.params.prop;
        let valor = req.params.valor;

        addProp(prop, newvalor)

        res.statusMessage = "Inserido com sucesso";
    }
  else{
      res.sendStatus(400);
  }
})
function addProp(prop, newValor){

}

function criarJogador(){
    let jogador = {
        "nome": dados.nome[Math.floor(Math.random() * dados.nome.length)],
        "sobrenome": dados.sobrenome[Math.floor(Math.random() * dados.sobrenome.length)],
        "idade": Math.floor(Math.random() * (40 - 17 + 1) + 17),
        "posicao": dados.posicao[Math.floor(Math.random() * dados.posicao.length)],
        "clube": dados.clube[Math.floor(Math.random() * dados.clube.length)],
    }
    return jogador;
};

