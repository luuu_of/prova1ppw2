const express = require('express')
const app=express()
const routes = require('./routes')
const PORT = process.env.PORT || 3000

app.get('/',function(req,res){
    res.send("por favor insira /listar para ver o conteúdo")
})

app.use('/api',routes)

app.listen(PORT,function(){
    console.log("Servidor online, porta:"+PORT)
})

